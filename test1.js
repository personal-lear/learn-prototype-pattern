function Document(title, author, content) {
    this.title = title;
    this.author = author;
    this.content = content;
}

// Thêm phương thức clone vào prototype của lớp Document
Document.prototype.clone = function() {
    // Tạo một đối tượng mới và sao chép các thuộc tính từ đối tượng gốc
    return new Document(this.title, this.author, this.content);
}

// Tạo một đối tượng Document mới
var originalDocument = new Document("Title 1", "Author 1", "Content 1");

// Sao chép đối tượng Document để tạo một tài liệu mới
var newDocument = originalDocument.clone();

// Thay đổi thông tin của tài liệu mới
newDocument.title = "New Title";
newDocument.author = "New Author";
newDocument.content = "New Content";

// Hiển thị thông tin của tài liệu mới
console.log("New Document Title:", newDocument.title);
console.log("New Document Author:", newDocument.author);
console.log("New Document Content:", newDocument.content);


function Image(src) {
    this.src = src;
    // Giả định có các thuộc tính và phương thức phức tạp khác
}

Image.prototype.clone = function (){
    return new Image(this.src);
}

// Load hình ảnh từ server
function loadImageFromServer(url) {
    // Giả định quá trình tải hình ảnh từ server
    return new Image(url);
}

// Tạo đối tượng hình ảnh từ server (giả sử là quá trình khởi tạo tốn kém)
var originalImage = loadImageFromServer("https://example.com/image.jpg");

// Tạo các đối tượng hình ảnh mới từ đầu mỗi khi cần
var image1 = originalImage.clone();
var image2 = originalImage.clone();
var image3 = originalImage.clone();