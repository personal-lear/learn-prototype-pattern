function Image(src) {
    this.src = src;
    // Giả định có các thuộc tính và phương thức phức tạp khác
}

// Load hình ảnh từ server
function loadImageFromServer(url) {
    // Giả định quá trình tải hình ảnh từ server
    return new Image(url);
}

// Tạo đối tượng hình ảnh từ server (giả sử là quá trình khởi tạo tốn kém)
var originalImage = loadImageFromServer("https://example.com/image.jpg");

// Tạo các đối tượng hình ảnh mới từ đầu mỗi khi cần
var image1 = loadImageFromServer(originalImage.src);
var image2 = loadImageFromServer(originalImage.src);
var image3 = loadImageFromServer(originalImage.src);