Mẫu Prototype trong lập trình là một mẫu thiết kế (design pattern) được sử dụng để tạo ra các đối tượng một cách linh hoạt và hiệu quả. Mục tiêu của mẫu này là để tạo ra các đối tượng mới bằng cách sao chép (clone) một đối tượng đã tồn tại, thay vì tạo ra một đối tượng mới từ đầu
mẫu Prototype vẫn là một công cụ hữu ích trong việc tạo ra và quản lý các đối tượng, đặc biệt trong các ứng dụng yêu cầu tạo ra nhiều đối tượng giống nhau hoặc có cấu trúc phức tạp
- Lợi ích
+ Giảm tải cho hệ thống tạo đối tượng: Đối với các đối tượng có cấu trúc phức tạp, việc tạo mới từng đối tượng từ đầu có thể tốn nhiều tài nguyên. Sử dụng Prototype, bạn có thể sao chép một đối tượng đã tồn tại để tạo ra các bản sao một cách hiệu quả hơn.

+ Tạo ra các đối tượng tùy chỉnh dựa trên các đối tượng sẵn có: Thay vì xây dựng một đối tượng từ đầu và định nghĩa lại các thuộc tính, bạn có thể sao chép một đối tượng gốc và điều chỉnh một số thuộc tính để tạo ra một đối tượng mới phù hợp với nhu cầu cụ thể.

+ Một số ngôn ngữ lập trình cũng hỗ trợ mẫu này cho việc quản lý bộ nhớ: Thay vì tạo ra các đối tượng mới, bạn có thể sao chép các đối tượng đã tồn tại, giúp giảm bớt gánh nặng cho bộ nhớ.

+ Tính linh hoạt trong việc mở rộng và sửa đổi cấu trúc của đối tượng: Khi bạn muốn mở rộng hoặc thay đổi cấu trúc của một đối tượng, việc sử dụng Prototype có thể dễ dàng hơn so với việc thay đổi trực tiếp đối tượng gốc.

+ Thêm một phương thức mới vào prototype của một lớp.

+ Làm rối rắm cấu trúc: Việc sử dụng mẫu Prototype có thể làm rối rắm cấu trúc của mã nếu không được sử dụng một cách cẩn thận. Khi có nhiều tùy chọn thay thế cho việc sao chép đối tượng, điều này có thể gây khó khăn trong việc theo dõi và hiểu mã nguồn.

+ Khả năng sao chép đối tượng không sâu: Khi một đối tượng chứa các tham chiếu đến các đối tượng khác, việc sao chép đối tượng đó thông qua mẫu Prototype chỉ tạo ra một bản sao của tham chiếu, không phải là một bản sao của đối tượng tham chiếu. Điều này có thể dẫn đến tình huống không mong muốn khi thay đổi dữ liệu của đối tượng gốc cũng làm thay đổi dữ liệu của bản sao.

