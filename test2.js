function Document(title, author, content) {
    this.title = title;
    this.author = author;
    this.content = content;
}

// Tạo một đối tượng Document mới
var originalDocument = new Document("Title 1", "Author 1", "Content 1");

// Sao chép đối tượng Document để tạo một tài liệu mới
var newDocument = new Document(originalDocument.title, originalDocument.author, originalDocument.content);

// Thay đổi thông tin của tài liệu mới
newDocument.title = "New Title";
newDocument.author = "New Author";
newDocument.content = "New Content";

// Hiển thị thông tin của tài liệu mới
console.log("New Document Title:", newDocument.title);
console.log("New Document Author:", newDocument.author);
console.log("New Document Content:", newDocument.content);


