class Dog {
    constructor(name) {
        this.name = name;
    }

    bark() {
        return `Woof!`;
    }
}

const dog1 = new Dog("Daisy");
const dog2 = new Dog("Max");
const dog3 = new Dog("Spot");

Dog.prototype.play = () => console.log("Playing now!");

dog1.play();
dog2.play();
dog3.play();



class SuperDog extends Dog {
    constructor(name) {
        super(name);
    }

    fly() {
        console.log(`Flying!`);
    }
}

const dog4 = new SuperDog("Daisy");
dog4.bark();
dog4.fly();

