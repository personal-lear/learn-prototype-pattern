function Address(city, country) {
    this.city = city;
    this.country = country;
}

// Định nghĩa lớp Person
function Person(name, address) {
    this.name = name;
    this.address = address;
}

// Tạo một đối tượng Address
var address = new Address("New York", "USA");

// Tạo một đối tượng Person với đối tượng Address được chia sẻ
var person1 = new Person("John", address);

// Sao chép đối tượng Person
var person2 = Object.assign({}, person1);

// Thay đổi địa chỉ trong person2
person2.address.city = "Los Angeles";

// Hiển thị thông tin của cả hai đối tượng Person
console.log("Person 1 Address:", person1.address.city); // Kết quả: Los Angeles
console.log("Person 2 Address:", person2.address.city);